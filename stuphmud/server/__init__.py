# A stuphMUD component.
# --
# New Role:
#    Multi-user networking library which also connects the programming application.
#
#    The important thing to note about this arrangement is that the classical (or
#	 legacy) 'mud' package is now side-by-side with the network.server modules.
#

# MUD Extension Package (Core).
    # The mud package does three things: it provides a standard python package
    # for running in-game routines from python; it provides the runtime which
    # supports the event bridge (from the game server perspective); and it provides
    # a library of feature implementation that itself is designed to be extended.

# from stuphos.runtime.core import *
